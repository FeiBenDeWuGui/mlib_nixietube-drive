/*UTF-8*/
#ifndef _MLIB_NIXIETUBEDRIVE_H_
#define _MLIB_NIXIETUBEDRIVE_H_

#ifndef uint8
#define uint8 unsigned char
#endif

#ifndef uint16
#define uint16 unsigned short int
#endif

#ifndef uint32
#define uint32 unsigned long
#endif

#ifndef int8
#define int8 signed char
#endif
#ifndef int16
#define int16 signed short int
#endif

#ifndef int32
#define int32 signed long
#endif


typedef union
{
    uint8 seg8_general[8];
}MLIB_NIXIETUBE_DISPLAY_BUFF_U;

typedef enum
{
    //常见8字形数码管，带小数点，共8段
    MLIB_NIXIETUBE_SEG8_GENERAL=0,
}MLIB_NIXIETUBE_TYPE_E;

/*
记录数码管扫描状态
*/
typedef struct
{
    MLIB_NIXIETUBE_TYPE_E type_enum;    //数码管类型
    uint8 bit_count;                    //数码管位数
    uint8 bit_enable_level;             //位使能电平   只能配置为0或1
    uint8 seg_enable_level;             //段使能电平   只能配置为0或1
    uint16 lum_level_max;                //最大亮度等级
    uint16 lum_level;                    //显示亮度等级
    
    MLIB_NIXIETUBE_DISPLAY_BUFF_U dis_buff;
    //以下参数，用户不关心
    uint8 scan_nowsit;         //记录当前扫描的位 位置
    uint8 scan_state; 
    uint16 time_ms;
}MLIB_NIXIETUBE_OBJECT_S;





void mlib_Nixietube_init(void(* hardwareInit)(void),
                    MLIB_NIXIETUBE_OBJECT_S *myobject);
void mlib_Nixietube_Object_DefaulInit(MLIB_NIXIETUBE_OBJECT_S *myobject);
void mlib_Nixietube_Scan(MLIB_NIXIETUBE_OBJECT_S *myobject,
                        void(* bitEnable)(uint8 intdex,uint8 state),
                        void(* segOutput)(uint16 segvalue));


void mlib_Nixietube_Sit_DisplayNumber(MLIB_NIXIETUBE_OBJECT_S *myobject,uint8 sit,uint8 number);

void mlib_Nixietube_DisplayInt16(MLIB_NIXIETUBE_OBJECT_S *myobject,int16 value);
void mlib_Nixietube_DisplayUint16(MLIB_NIXIETUBE_OBJECT_S *myobject,uint16 value);
void mlib_Nixietube_DisplayUint16_Custom(MLIB_NIXIETUBE_OBJECT_S *myobject,uint8 startSit,uint16 value,uint8 maxSit);
                        
void mlib_Nixietube_DisplayFloat(MLIB_NIXIETUBE_OBJECT_S *myobject,float value,uint8 point);

void mlib_Nixietube_lum_level_set(MLIB_NIXIETUBE_OBJECT_S *myobject,uint16 value);




#endif
