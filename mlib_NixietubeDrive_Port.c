#include "mlib_NixietubeDrive_Port.h"



const MCULIB_GPIO_STRUCT LEDA=
{RCC_AHB1Periph_GPIOD,GPIOD,GPIO_Pin_2,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDB=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_3,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDC=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_8,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDD=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_6,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDE=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_5,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDF=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_4,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDG=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_12,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT LEDH=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_7,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};





const MCULIB_GPIO_STRUCT CS1=
{RCC_AHB1Periph_GPIOB,GPIOB,GPIO_Pin_9,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS2=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_13,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS3=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_14,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS4=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_15,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS5=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_0,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS6=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_1,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS7=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_2,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};
const MCULIB_GPIO_STRUCT CS8=
{RCC_AHB1Periph_GPIOC,GPIOC,GPIO_Pin_3,GPIO_Mode_OUT,GPIO_Speed_25MHz,GPIO_OType_PP,GPIO_PuPd_NOPULL};

void nixietube_drive_io_init()
{
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDA);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDB);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDC);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDD);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDE);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDF);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDG);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&LEDH);
    
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS1);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS2);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS3);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS4);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS5);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS6);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS7);
    mculib_gpio_init((MCULIB_GPIO_STRUCT *)&CS8);
}

void test_iox()
{
    mculib_gpio_not((MCULIB_GPIO_STRUCT *)&CS1);
}

void sit_set(uint8 sit,uint8 state)
{
    switch(sit)
    {
        case 0:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS1,state);break;
        case 1:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS2,state);break;
        case 2:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS3,state);break;
        case 3:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS4,state);break;
        case 4:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS5,state);break;
        case 5:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS6,state);break;
        case 6:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS7,state);break;
        case 7:mculib_gpio_write((MCULIB_GPIO_STRUCT *)&CS8,state);break;
        default:break;
    }
}
void seg_set(uint16_t value)
{
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDA,value&0x01);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDB,value&0x02);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDC,value&0x04);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDD,value&0x08);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDE,value&0x10);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDF,value&0x20);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDG,value&0x40);
    mculib_gpio_write((MCULIB_GPIO_STRUCT *)&LEDH,value&0x80);
}













