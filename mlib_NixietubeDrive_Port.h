#ifndef _MLIB_NIXIETUBEDRIVE_PORT_H_
#define _MLIB_NIXIETUBEDRIVE_PORT_H_


#include "mculib_gpio.h"


void nixietube_drive_io_init();

void sit_set(uint8_t sit,uint8_t state);
void seg_set(uint16_t value);

void test_iox();





#endif
