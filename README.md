# mlib_NixietubeDrive

#### 介绍
用于驱动数码管的上层库函数，只需要提供底层IO控制，即可实现数码管的扫描，可以很方便的配置扫描频率，
以及不同位数的数码管，后续将会支持不同类型的数码管。

1.当前版本仅仅支持普通的8字形数码管（带小数点或者不带小数点）


#### 软件架构
C  32位单片机

#### 使用说明

将仓库复制到项目中，然后添加三个文件

    mlib_NixietubeDrive.c
    mlib_NixietubeDrive.h
    mlib_NixietubeDrive_Port.c

需要根据不同的单片机修改或实现mlib_NixietubeDrive_Port.c中的两个函数：

    void sit_set(uint8 sit,uint8 state)
    void seg_set(uint16 value)


#### 使用步骤

1.定义一个数码管对象

    MLIB_NIXIETUBE_OBJECT_S myobject;

2.使用默认参数初始化数码管对象

    mlib_Nixietube_Object_DefaulInit(&myobject);

3.初始化这个数码管对象,以及底层IO，需要传递一个初始化底层IO的函数指针

    mlib_Nixietube_init(hardwareInit,&myobject);

4.重新配置数码管对象的参数（如果使用函数mlib_Nixietube_Object_DefaulInit默认配置过参数，
而你也没有自定义参数的特殊需求，则此步可以跳过）

    myobject->type_enum=MLIB_NIXIETUBE_SEG8_GENERAL;//设置数码管的类型，比如普通8位，普通7位或者米字型数码管等等
    myobject->bit_count=4; //设置数码管总共有多少位
    myobject->bit_enable_level=0;//使能数码管位选是高电平还是低电平
    myobject->seg_enable_level=0;//使能数码管段选是低电平还是高电平
    myobject->lum_level_max=50; //最大亮度等级
    myobject->lum_level=0;      //当前亮度等级
    myobject->dis_buff.seg8_general[0]=display_value[0]; //默认显示的内容
    myobject->dis_buff.seg8_general[1]=display_value[1];
    myobject->dis_buff.seg8_general[2]=display_value[2];
    myobject->dis_buff.seg8_general[3]=display_value[3];
    
5.在接口文件：mlib_NixietubeDrive_Port.c 中实现位选和段选的底层IO控制函数


6.在中断或者定时器中调用扫描函数，调用频率和数码管的刷新频率息息相关，具体的计算公式如下。

    mlib_Nixietube_Scan(&myobject,sit_set,seg_set);
    
    扫描频率计算方法：
    
    Hz=   1/(扫描函数调用时间*(最大亮度等级+当前亮度等级)*数码管位数)
    
    例如：扫描函数每100个us调用一次，最大亮度等级为40，当前亮度等级为10 数码管4位
        扫描频率=1 / ( 0.0001s*(40+10)*4 )= 50Hz

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
